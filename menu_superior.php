<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {margin:0;}

.navbar {
  overflow: hidden;
  background-color: #333;
  position: fixed;
  top: 0;
  width: 100%;
}

.navbar a {
  float: right;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.navbar a:hover {
  background: #ddd;
  color: black;
}

.main {
  padding: 16px;
  margin-top: 30px;
  height: 1500px; /* Used in this example to enable scrolling */
}

#logo {
  float: left;
  display: block;
  text-align: center;
  padding: 0px 16px;
  text-decoration: none;
  height: 48px;
}

#logo:hover {
  background: #333;
}


</style>
</head>
<body>

<div class="navbar">
  <a id="logo" href="#start"> <img src="el_logo_mini.png" height=50></a>
  <a href="#opendocman">WordPress</a>
   <a href="#opendocman">ProcessMaker</a>
  <a href="#opendocman">OpenDocMan</a>
  <a href="#mediawiki">MediaWiki</a>
  <a href="#kanboard">Kanboard</a>
  <a href="#gitlab">GitLab</a>
  <a href="#home">Home</a>
 
</div>

<div class="main">
  <h1>Fixed Top Menu</h1>
  <h2>Scroll this page to see the effect</h2>
  <h2>The navigation bar will stay at the top of the page while scrolling</h2>

  <p>Some text some text some text some text..</p>
  <p>Some text some text some text some text..</p>
  <p>Some text some text some text some text..</p>
  <p>Some text some text some text some text..</p>

</div>

</body>
</html>
