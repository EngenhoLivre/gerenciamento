Desenvovimento de uma ferramenta de integração entre os projetos:
- WordPress (apresentação e divulgação)
- MediaWiki (gestão do conhecimento)
- Kanboard (gerenciamento de projetos)
- GitLab (desenvolvimento de software)
- Laravel (framework de desenvolvimento Web)

Utilizando as seguintes tecnologias:
- HTML5
- CSS
- PHP
- Node.js
- MariaDB
- Sqlite
- Python